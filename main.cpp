﻿#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <functional>
#include "types.h"
#include "decompress.h"
#include "compress.h"

using namespace std;

#define worst_ratio 1.02
// Do not increase this limit, will lead to int overflow
const int MAX_INPUT_SIZE = int(INT_MAX / 8 / worst_ratio);

void PrintAbout() {
	printf("\n");
	printf("RIP compressor v2023.03.30");
	if (sizeof(void*) > 4) {
		printf(" (x64)\n");
	} else {
		printf(" (x86)\n");
	}
	printf("by Eugene Larchenko <el6345@gmail.com> (https://gitlab.com/eugene77)\n");
	printf("\n");
}

void PrintUsage() {
	printf("Usage:\n");
	printf("  rip.exe [-d] [-rb] [--max-offset N] [--max-len N] <inputfilename> [<outputfilename>]\n");
	printf("    -d              = decompress (default is compress)\n");
	printf("    -rb             = reverse bits in compressed data\n");
	printf("    --max-offset N  = max reference offset\n");
	printf("    --max-len N     = max reference len\n");
	printf("\n");
}

int ReadMaxCopyOffset(string arg) {
	bool isNum = (arg.size() > 0);
	for(char c : arg) {
		isNum &= ('0' <= c && c <= '9');
	}

	if (!isNum) {
		PrintUsage();
		printf("Invalid arguments\n");
		throw 1;
	}

	int value = atoi(arg.data());

	auto possible_values = get_offset_boundary_values();
	bool found = false;
	for(auto x : possible_values) {
		found |= (x == value);
	}

	if (found) {
		return value;
	} else {
		printf("Invalid arguments. Possible values for --max-offset are:\n");
		for(auto x : possible_values) {
			printf((x == 0 ? "%d" : ", %d"), x);
		}
		printf("\n");
		throw 1;
	}
}

int ReadMaxCopyLen(string arg) {
	bool isNum = (arg.size() > 0);
	for(char c : arg) {
		isNum &= ('0' <= c && c <= '9');
	}

	if (!isNum) {
		PrintUsage();
		printf("Invalid arguments\n");
		throw 1;
	}

	int value = atoi(arg.data());

	auto possible_values = get_offset_boundary_values();
	const int MAX_LEN_CORRECTION = 1;
	if (0 <= value && value <= possible_values.back() + MAX_LEN_CORRECTION) {
		return value;
	} else {
		printf("Invalid arguments. Possible values for --max-len are 0..%d\n", 
			possible_values.back() + 1);
		throw 1;
	}
}

bool file_exists(const char* path)
{
	FILE* f = fopen(path, "r");
	if (f) {
		fclose(f);
	}
	return (f != NULL);
}

int main(int argc, char* argv[])
{
	int retCode = 10;
	string outPath = "\0";
	bool deleteResult = false;
	try
	{
		PrintAbout();

		// Read options
		char mode = 'c'; // compress
		bool reverseBits = false;
		int max_copy_offset = MAX_INPUT_SIZE;
		int max_copy_len = MAX_INPUT_SIZE;
		bool showStats = false;
		int a = 1;
		while(a < argc) {
			if (strcmp(argv[a], "-d") == 0) {
				mode = 'd';
				a++;
			}
			else if (strcmp(argv[a], "-rb") == 0) {
				reverseBits = true;
				printf("Using reverse bits in compressed data\n");
				a++;
			}
			else if (strcmp(argv[a], "--max-offset") == 0) {
				a++;
				if (a >= argc) {
					break;
				}
				max_copy_offset = ReadMaxCopyOffset(string(argv[a]));
				showStats = true;
				a++;
				printf("Using max copy offset: %d\n", max_copy_offset);
			}
			else if (strcmp(argv[a], "--max-len") == 0) {
				a++;
				if (a >= argc) {
					break;
				}
				max_copy_len = ReadMaxCopyLen(string(argv[a]));
				showStats = true;
				a++;
				printf("Using max copy len: %d\n", max_copy_len);
			}
			else {
				break;
			}
		}

		// Make input and output file paths
		if (a >= argc) {
			PrintUsage();
			printf("Invalid arguments\n");
			throw 1;
		}
		string inPath = argv[a++];
		inPath += "\0";
		outPath = inPath;
		if (a < argc) {
			outPath = argv[a++];
			outPath += "\0";
		} else {
			outPath += (mode == 'c' ? ".rip\0" : ".unrip\0");
		}

		if (file_exists(outPath.c_str())) {
			// we don't want overwriting file. Don't waste time, abort now.
			printf("Error: output file already exists: %s\n", outPath.c_str());
			throw 2;
		}

		FILE* fIn = fopen(inPath.c_str(), "rb");
		if (!fIn) {
			printf("Error opening file %s\n", inPath.c_str());
			throw 2;
		}

		printf("Reading input file %s\n", inPath.c_str());
		vector<byte> data;
		while(true) {
			byte buf[4096];
			size_t q = fread(buf, 1, 4096, fIn);
			if (q <= 0) {
				break;
			}
			data.reserve(data.size() + q);
			for(size_t i=0; i<q; i++) {
				data.push_back(buf[i]);
			}
			if (data.size() > MAX_INPUT_SIZE) {
				throw exception("Input file is too large");
			}
		}
		
		fclose(fIn); fIn = NULL;

		std::function<void(vector<byte>&)> saveResult = [&](vector<byte>& result) {
			remove(outPath.c_str());
			deleteResult = true;
			FILE* fOut = fopen(outPath.c_str(), "wb");
			if (!fOut) {
				printf("Error creating output file %s\n", outPath.c_str());
				throw 3;
			}
			size_t written = fwrite(result.data(), 1, result.size(), fOut);
			if (written != result.size()) {
				printf("Error writing output file\n");
				throw 3;
			}
			fclose(fOut);
		};

		printf("Processing\n");

		size_t datasize = data.size();
		if (mode == 'c')
		{
			// repeat the data; required for Z-function
			data.reserve(datasize * 2);
			for(size_t i=0; i < datasize; i++) {
				byte t = data[i]; data.push_back(t);
			}
		}

		PackResult packResult;
		vector<byte> result = mode=='c' 
			? (packResult = compress(data, (int)datasize, reverseBits, max_copy_offset, max_copy_len, saveResult))
				.PackedData
			: decompress(data, reverseBits);

		//printf("time=%f\n", timeSpent);
		if (showStats) {
			printf("Max copy offset used = %d\n", packResult.MaxCopyOffset);
			printf("Max copy len used = %d\n", packResult.MaxCopyLen);
		}

		printf("Writing result to %s\n", outPath.c_str());
		saveResult(result);
		deleteResult = false;

		printf("All OK\n");
		retCode = 0;
	}
	catch (int code)
	{
		retCode = code;
	}
	catch (std::bad_alloc& e)
	{
		printf("ERROR: Couldn't allocate memory (%s)\n", e.what());
		retCode = 8;
	}
	catch (std::exception& e)
	{
		printf("ERROR: %s\n", e.what());
		retCode = 9;
	}

	if (deleteResult) {
		// delete incomplete compressed file
		remove(outPath.c_str());
	}

	return retCode;
}
