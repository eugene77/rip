﻿# RIP data compressor

RIP aka Real Information Packer is a data compressor originally developed by Roman Petrov on ZX Spectrum platform. It implements comprehensive compression scheme (LZ + Huffman + offset reuse; see [format description](format_description.txt)) while keeping decompressor size small.

This project contains powerful compressor implementation for PC platform. This implementation uses dynamic programming + exhaustive search with heuristics to achieve compression level close to optimal for RIP coding scheme.

The compressor is rather slow and has the following **limitations**:

*  severe amounts of memory -- up to 4\**N*<sup>2</sup> bytes -- required; for this reason 64-bit mode is recommended;
*  it is unlikely to swallow files larger than 100 kB.

This compressor produces raw compressed stream, no headers, no decompressor included. 

Option `-rb` reverses bits in compressed stream (asked by community).

Option `--max-offset` limits the offset for copy codes.
Option `--max-len` limits the length for copy codes.

### Decompressor

Use `-d` option to decompress files.

[z80](z80) directory contains a few decompressor implementations for Z80 platform. All implementations require working area of 1474 bytes.

### Legal

(c) 2022-2023 Eugene Larchenko <el6345@gmail.com>
Compressor is published under [custom license](LICENSE). Z80 decompressors are published without license.

### See also

* [mRIP](https://gitlab.com/eugene77/mrip) - simplified version of RIP compressor
* [RIP decompressor for i8080](https://github.com/ivagorRetrocomp/DeRIP)
