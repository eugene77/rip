﻿#pragma once

#include "types.h"
#include <vector>
#include <functional>

using namespace std;

extern double timeSpent;

struct PackResult
{
	vector<byte> PackedData;
	vector<int> ResultCodeStats, ResultOffsetStats;
	int MaxCopyOffset;
	int MaxCopyLen;
};


extern PackResult compress(
	const vector<byte>& data_x2, int datasize_x1,
	bool reverseBits,
	int manual_offset_limit,
	int manual_copylen_limit,
	std::function<void(vector<byte>&)>& saveResult
);

extern vector<ushort> get_offset_boundary_values();
